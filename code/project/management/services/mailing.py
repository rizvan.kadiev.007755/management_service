from datetime import datetime
from django.utils import timezone
from management.models import Mailing, Client, Message
from management.services.client_filter import ClientFilterService
from management.tasks import ManagementTask
import logging

logger = logging.getLogger(__name__)


class MailingService:

    @staticmethod
    def schedule_mailing(mailing_id):
        try:
            mailing = Mailing.objects.get(id=mailing_id)
            current_time = timezone.now()
            if current_time < mailing.start_date:
                delay = (mailing.start_date - current_time).total_seconds()
                logger.info(f"Запланировано выполнение рассылки через {delay} секунд")
                ManagementTask.execute_mailing.apply_async((mailing_id,), countdown=delay)
            else:
                logger.info("Рассылка запускается немедленно")
                ManagementTask.execute_mailing(mailing_id)
        except Exception as e:
            logger.error(f"Ошибка при планировании рассылки: {e}")
