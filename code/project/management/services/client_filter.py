from management.models import Client


class ClientFilterService:

    @staticmethod
    def filter_clients(operator_code=None, tag=None):
        filters = {}
        if operator_code:
            filters['operator_code'] = operator_code
        if tag:
            filters['tag'] = tag
        return Client.objects.filter(**filters)
