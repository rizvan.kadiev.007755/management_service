from django.contrib import admin
from management.models import Mailing, Message, Client

admin.site.register(Mailing)
admin.site.register(Message)
admin.site.register(Client)
