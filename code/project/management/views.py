from rest_framework import viewsets
from management.models import Mailing
from management.serializers import MailingSerializer
from management.services.mailing import MailingService


class MailingViewSet(viewsets.ModelViewSet):
    queryset = Mailing.objects.all()
    serializer_class = MailingSerializer

    def perform_create(self, serializer):
        mailing = serializer.save()
        MailingService.schedule_mailing(mailing.id)
