from django.test import TestCase
from pytz import timezone
from datetime import datetime
from unittest.mock import patch, Mock
from management.tasks import ManagementTask
from rest_framework.test import APIClient
from .models import Client, Mailing
from rest_framework import status


class MailingAPITest(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.client_instance = Client.objects.create(
            phone_number="1234567890",
            operator_code="12345",
            tag="тест"
        )

    def format_datetime(self, dt_str):
        dt = datetime.strptime(dt_str, '%Y-%m-%dT%H:%M:%S.%f%z')
        return dt.strftime('%Y-%m-%dT%H:%M:%S.%f%z')

    def test_create_mailing_current_time(self):
        moscow_timezone = timezone('Europe/Moscow')
        current_time = datetime.now(moscow_timezone)

        mailing_data = {
            "start_date": current_time,
            "end_date": current_time,
            "message_text": "Тестовое сообщение",
            "operator_code": "12345",
            "tag": "тест"
        }

        response = self.client.post('/mailings/', mailing_data)

        self.assertEqual(response.status_code, 201)

        mailing_data = {
            "id": 1,
            "start_date": current_time,
            "end_date": current_time,
            "message_text": "Тестовое сообщение",
            "operator_code": "12345",
            "tag": "тест"
        }
        mailing_data["start_date"] = mailing_data["start_date"].strftime('%Y-%m-%dT%H:%M:%S.%f%z')
        mailing_data["end_date"] = mailing_data["end_date"].strftime('%Y-%m-%dT%H:%M:%S.%f%z')
        mailing_data["start_date"] = mailing_data["start_date"].replace("+0300", "+03:00")
        mailing_data["end_date"] = mailing_data["end_date"].replace("+0300", "+03:00")
        self.assertEqual(response.data, mailing_data)

    def test_get_all_mailings(self):
        mailing1 = Mailing.objects.create(
            start_date="2024-01-23T15:00:00+03:00",
            end_date="2024-01-23T15:30:00+03:00",
            message_text="Тестовое сообщение 1",
            operator_code="12345",
            tag="тест1"
        )
        mailing2 = Mailing.objects.create(
            start_date="2024-01-23T16:00:00+03:00",
            end_date="2024-01-23T16:30:00+03:00",
            message_text="Тестовое сообщение 2",
            operator_code="67890",
            tag="тест2"
        )

        response = self.client.get('/mailings/')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)
        self.assertEqual(response.data[0]['operator_code'], '12345')
        self.assertEqual(response.data[0]['tag'], 'тест1')
        self.assertEqual(response.data[1]['operator_code'], '67890')
        self.assertEqual(response.data[1]['tag'], 'тест2')

        self.assertEqual(response.data[0]['start_date'], '2024-01-23T15:00:00+03:00')
        self.assertEqual(response.data[1]['start_date'], '2024-01-23T16:00:00+03:00')
        self.assertEqual(response.data[0]['end_date'], '2024-01-23T15:30:00+03:00')
        self.assertEqual(response.data[1]['end_date'], '2024-01-23T16:30:00+03:00')


class ManagementTaskTest(TestCase):
    @patch('management.tasks.Mailing.objects.get')
    @patch('management.tasks.ClientFilterService.filter_clients')
    @patch('management.tasks.Message.objects.create')
    @patch('management.tasks.logger.info')
    def test_execute_mailing(self, mock_logger_info, mock_message_create, mock_filter_clients, mock_get):
        moscow_timezone = timezone('Europe/Moscow')
        current_time = datetime.now(moscow_timezone)

        mock_mailing = Mock()
        mock_client = Mock()

        mock_mailing.start_date = current_time
        mock_mailing.end_date = current_time
        mock_mailing.operator_code = "12345"
        mock_mailing.tag = "тест"
        mock_mailing.message_text = "Тестовое сообщение"

        mock_get.return_value = mock_mailing
        mock_filter_clients.return_value = [mock_client]

        ManagementTask.execute_mailing(1)

        mock_get.assert_called_once_with(id=1)
        mock_filter_clients.assert_called_once_with("12345", "тест")
        mock_message_create.assert_called_once_with(mailing=mock_mailing, client=mock_client)
        mock_logger_info.assert_called_with(
            f"Сообщение 'Тестовое сообщение' логировано для клиента {mock_client.phone_number}")
