from datetime import datetime
from django.utils import timezone
from management.models import Mailing, Client, Message
from management.services.client_filter import ClientFilterService
from celery import shared_task
import logging

logger = logging.getLogger(__name__)


class ManagementTask:

    @staticmethod
    @shared_task
    def execute_mailing(mailing_id):
        try:
            mailing = Mailing.objects.get(id=mailing_id)
            if mailing.start_date <= mailing.end_date:
                clients = ClientFilterService.filter_clients(mailing.operator_code, mailing.tag)
                logger.info(f"Найдено клиентов: {len(clients)}")
                for client in clients:
                    Message.objects.create(mailing=mailing, client=client)
                    logger.info(f"Сообщение '{mailing.message_text}' логировано для клиента {client.phone_number}")
            else:
                logger.info("Условие времени не выполнено")
        except Exception as e:
            logger.error(f"Ошибка при выполнении рассылки: {e}")
