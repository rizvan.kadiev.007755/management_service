from django.urls import path, include
from rest_framework.routers import DefaultRouter
from management.views import MailingViewSet

router = DefaultRouter()
router.register(r'mailings', MailingViewSet)

urlpatterns = [
    path('', include(router.urls)),
]
