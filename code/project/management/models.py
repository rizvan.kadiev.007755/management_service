from django.db import models


class Mailing(models.Model):
    start_date = models.DateTimeField(verbose_name='Дата начала')
    end_date = models.DateTimeField(verbose_name='Дата окончания')
    message_text = models.TextField(verbose_name='Текст сообщения')
    operator_code = models.CharField(max_length=10, blank=True, null=True, verbose_name='Код оператора')
    tag = models.CharField(max_length=50, blank=True, null=True, verbose_name='Тег')

    def __str__(self):
        return f"Рассылка {self.id} от {self.start_date.strftime('%Y-%m-%d %H:%M')}"

    class Meta:
        verbose_name = "Рассылка"
        verbose_name_plural = "Рассылки"


class Client(models.Model):
    phone_number = models.CharField(max_length=12, unique=True, verbose_name='Номер телефона')
    operator_code = models.CharField(max_length=10, verbose_name='Код оператора')
    tag = models.CharField(max_length=50, blank=True, null=True, verbose_name='Тег')

    def __str__(self):
        return f"Клиент {self.phone_number}"

    class Meta:
        verbose_name = "Клиент"
        verbose_name_plural = "Клиенты"


class Message(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')
    mailing = models.ForeignKey(Mailing, on_delete=models.CASCADE, verbose_name='Рассылка')
    client = models.ForeignKey(Client, on_delete=models.CASCADE, verbose_name='Клиент')

    def __str__(self):
        return f"Сообщение от {self.created_at.strftime('%Y-%m-%d %H:%M')} к рассылке {self.mailing.id}"

    class Meta:
        verbose_name = "Сообщение"
        verbose_name_plural = "Сообщения"
